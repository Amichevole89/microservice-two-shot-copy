import React, { useState, useEffect } from "react";

const ShoeList = () => {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState([]);
  const url = "http://localhost:8080/api/shoes/";

  const deletedShoe = async (href) => {
    fetch(`http://localhost:8080${href}`, {
      method: "DELETE",
      headers: { "Content-Type": "application/json" },
    }).then(() => {
      window.location.reload();
    });
  };

  useEffect(() => {
    setLoading(true);
    const fetchData = async () => {
      try {
        const response = await fetch(url);
        if (response.ok) {
          const json = await response.json();
          // console.log("shoe:", json);

          setLoading(false);
          setData(json.shoes);
        }
      } catch (e) {
        console.log("error", e);
      }
    };

    fetchData();
  }, []);

  return (
    <div className='container-fluid'>
      {loading && (
        <div>
          {" "}
          <h1>Loading...</h1>
        </div>
      )}
      <h1>Shoes</h1>
      <table className='table table-striped'>
        <thead>
          <tr>
            <th>Picture</th>
            <th>Manufacturer</th>
            <th>Model</th>
            <th>Color</th>
            <th>Bin</th>
            <th>Delete Shoe</th>
          </tr>
        </thead>
        <tbody>
          {/* {console.log(data)} */}
          {data.map((shoe) => (
            <tr key={shoe.href}>
              <td>
                <img
                  src={shoe.picture_url}
                  alt='loading failed'
                  height='75px'
                  width='100px'
                />
              </td>
              <td>{shoe.manufacturer}</td>
              <td>{shoe.model_name}</td>
              <td>{shoe.color}</td>
              <td>{shoe.bin}</td>
              <td>
                <button
                  onClick={() => deletedShoe(shoe.href)}
                  type='button'
                  className='btn btn-outline-danger'
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      <a href='http://localhost:3000/shoes/new'>
        <button className='btn btn-success'> Create Shoe</button>
      </a>
    </div>
  );
};
export default ShoeList;
