import React from "react";

class HatsList extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            hats: []
        };
    }

    async componentDidMount() {
        const url = ('http://localhost:8090/api/hats/');
        const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                this.setState({hats: data.hats});
                console.log(data.hats)
            }
    }
    async delete(id)
    {
      const deleteHatURL = `http://localhost:8090/api/hats/${id}`
      const fetchConfig = {
        method: "delete"
      }
      const deleteResponse = await fetch(deleteHatURL, fetchConfig)
      if (deleteResponse.ok){
        const deleted = await deleteResponse.json()
        console.log(deleted)
        window.location.reload(false)
      }
    }
    
    render() {
        return (
            <div className="container">
                <table className="table table-striped">
                    <thead>
                        <tr>
                            <th>Fabric</th>
                            <th>Style</th>
                            <th>Color</th>
                            <th>Location</th>
                            <th>Image</th>
                            <th>Delete</th>
                            <th>
                                <a href="http://localhost:3000/hats/new">  
                                    <button>Create Hat</button>  
                                </a>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.hats.map(hat => {
                            return (
                                <tr key={hat.id}>
                                    <td>{hat.fabric}</td>
                                    <td>{hat.style}</td>
                                    <td>{hat.color}</td>
                                    <td>{hat.location.closet_name}</td>
                                    <td><img alt=""className="photo" width="100px" height="auto" src={hat.picture_url}/> </td>
                                    <td><button className="btn btn-danger" onClick={() => this.delete(hat.id)}>Delete</button></td>
                                </tr>
                            )
                        }
                            )}
                    </tbody>
                </table>
            </div>
        );
    }
}

export default HatsList;
