import React, { useState, useEffect } from "react";

const ShoeForm = () => {
  const [state, setField] = useState({
    manufacturer: "",
    model_name: "",
    color: "",
    picture_url: "",
    bin: "",
    bins: [],
  });
  //   const [value, setValue] = useState("");
  // const [submitted, setSubmitted] = useState(false);
  const binListUrl = "http://localhost:8100/api/bins/";

  const handleInputChange = (e) => {
    const name = e.target.name;
    const value = e.target.value;
    setField({ ...state, [name]: value });
  };

  useEffect(() => {
    const fetchData = async (e) => {
      try {
        const response = await fetch(binListUrl);
        if (response.ok) {
          const data = await response.json();
          setField(s=> ({ ...s, bins: data.bins }));
        }
      }catch(e) {
        console.log("error", e);
      }
    };
    fetchData();

  }, []);

    const handleSubmit = async (e) => {
      e.preventDefault();
      const data = {...state}
      delete data.bins
      await fetch(`http://localhost:8080/api/shoes/`, {
        method: "POST",
        body:JSON.stringify(data),
        headers: {"Content-Type": "application/json"},

      })
      .then(() => {
        setField({...state})
        // window.history.back();
        window.location.href='/shoes';
     
      })
    };

  return (
    <div className='container'>
      <div className='row'>
        <div className='offset-3 col-6'>
          <div className='shadow p-4 mt-4'>
            <h1>Create a new shoe</h1>
            <form onSubmit={handleSubmit} className='form'>
              {/* {submitted ? (
                <div className='success-message'> Success!</div>
              ) : null} */}
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={state.manufacturer}
                  placeholder='Manufacturer'
                  required
                  type='text'
                  name='manufacturer'
                  id='manufacturer'
                  className='form-control'
                />
                <label htmlFor='manufacturer'>Manufacturer</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={state.model_name}
                  placeholder='Model'
                  required
                  type='text'
                  name='model_name'
                  id='model_name'
                  className='form-control'
                />
                <label htmlFor='model_name'>Model</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={state.color}
                  placeholder='Color'
                  required
                  type='text'
                  name='color'
                  id='color'
                  className='form-control'
                />
                <label htmlFor='color'>Color</label>
              </div>
              <div className='form-floating mb-3'>
                <input
                  onChange={handleInputChange}
                  value={state.picture_url}
                  placeholder='Picture'
                  required
                  type='text'
                  name='picture_url'
                  id='picture_url'
                  className='form-control'
                />
                <label htmlFor='picture_url'>Picture</label>
              </div>
              <div className='mb-3'>
                <select
                  onChange={handleInputChange}
                  value={state.bin}
                  placeholder='Bin'
                  required
                  type='text'
                  name='bin'
                  id='bin'
                  className='form-control'
                >
                  <option value=''>Choose a bin</option>
                  {state.bins.map((bin) => {
                    return (
                      <option key={[bin.href]} value={[bin.href]}>
                        {bin.closet_name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className='btn btn-outline-success'>Create</button>
              <button
                className='btn btn-outline-danger'
                type='reset'
              >
                Reset
              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShoeForm;

// import React from "react";

// class ShoeForm extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       manufacturer: "",
//       model_name: "",
//       color: "",
//       picture_url: "",
//       bins: [],
//     };
//     this.handleSubmit = this.handleSubmit.bind(this);
//   }
//   handleInputChange = (e) => {
//     const name = e.target.name;
//     const value = e.target.value;
//     this.setState({ ...this.state, [name]: value });
//   };
//   async handleSubmit(e) {
//     e.preventDefault();
//     const data = { ...this.state };
//     data.bins = data.bin;
//     const binListUrl = "http://localhost:8100/api/bins/";
//     const fetchConfig = {
//       method: "POST",
//       body: JSON.stringify(data),
//       headers: { "Content-Type": "application/json" },
//     };
//     const response = await fetch(binListUrl, fetchConfig);
//     if (response.ok) {
//       const cleared = {
//         manufacturer: "",
//         model_name: "",
//         color: "",
//         picture_url: "",
//         bin: "",
//       };
//       this.setState(cleared);
//     }
//   }

//   async componentDidMount() {
//     const url = "http://localhost:8080/api/shoes/";
//     const response = await fetch(url);
//     if (response.ok) {
//       const data = await response.json();
//       this.setState({ bins: data.bins });
//     }
//   }

//   render() {
//     return (
//       <div className='container'>
//         <div className='row'>
//           <div className='offset-3 col-6'>
//             <div className='shadow p-4 mt-4'>
//               <h1>Create a new shoe</h1>
//               <form className='form'>
//                 <div className='form-floating mb-3'>
//                   <input
//                     onChange={this.handleInputChange}
//                     value={this.state.manufacturer}
//                     placeholder='Manufacturer'
//                     required
//                     type='text'
//                     name='manufacturer'
//                     id='manufacturer'
//                     className='form-control'
//                   />
//                   <label htmlFor='manufacturer'>Manufacturer</label>
//                 </div>
//                 <div className='form-floating mb-3'>
//                   <input
//                     onChange={this.handleInputChange}
//                     value={this.state.model_name}
//                     placeholder='Model'
//                     required
//                     type='text'
//                     name='model_name'
//                     id='model_name'
//                     className='form-control'
//                   />
//                   <label htmlFor='model_name'>Model</label>
//                 </div>
//                 <div className='form-floating mb-3'>
//                   <input
//                     onChange={this.handleInputChange}
//                     value={this.state.color}
//                     placeholder='Color'
//                     required
//                     type='text'
//                     name='color'
//                     id='color'
//                     className='form-control'
//                   />
//                   <label htmlFor='color'>Color</label>
//                 </div>
//                 <div className='form-floating mb-3'>
//                   <input
//                     onChange={this.handleInputChange}
//                     value={this.state.picture_url}
//                     placeholder='Picture'
//                     required
//                     type='text'
//                     name='picture_url'
//                     id='picture_url'
//                     className='form-control'
//                   />
//                   <label htmlFor='picture_url'>Picture</label>
//                 </div>
//                 <div className='mb-3'>
//                   <select
//                     onChange={this.handleInputChange}
//                     value={this.state.bin}
//                     placeholder='Bin'
//                     required
//                     type='text'
//                     name='bin'
//                     id='bin'
//                     className='form-control'
//                   >
//                     <option value=''>Choose a bin</option>
//                     {this.state.bins.map((bin) => {
//                       return (
//                         <option key={bin.href} value={bin.href}>
//                           {bin.closet_name}
//                         </option>
//                       );
//                     })}
//                   </select>
//                 </div>
//               </form>
//             </div>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// export default ShoeForm;
