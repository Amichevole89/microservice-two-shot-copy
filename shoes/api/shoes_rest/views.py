from tkinter.messagebox import showerror
from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse

from common.json import ModelEncoder
from .models import BinVO, Shoe

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "import_href",
        "closet_name", 
        "bin_size",
        "id",
    ]
    def get_extra_data(self, o):
        return {"bin_number": o.id}
    

class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name", 
        "color",
        "picture_url",
        "id",
        "bin",
        ]
    # encoders = {
    #     "bin": BinVODetailEncoder(),
    # }
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "picture_url",
        "id",
  
    ]
    # encoders = {
    #     "bin": BinVODetailEncoder(),
    # }
    def get_extra_data(self, o):
        return {"bin": o.bin.closet_name}


@require_http_methods(["GET", "POST"])
def list_shoes(request, bin_vo_id=None):
    if request.method == "GET":
        if bin_vo_id is not None:
            shoes = Shoe.objects.filter(bin=bin_vo_id)
        else:
            shoes = Shoe.objects.all()

        return JsonResponse(
            {"shoes": shoes}, 
            encoder=ShoeListEncoder,
        )
    else:
        content = json.loads(request.body)
        # print(content)
        try:
            href = content["bin"]
            print(href)
            bin = BinVO.objects.get(import_href=href)
            content["bin"] = bin

        except BinVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid bin id, verify the information and try again!"},
                status=400,
            )
        shoes = Shoe.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoeListEncoder,
            safe=False,
        )

@require_http_methods(["GET", "PUT", "DELETE"])
def shoe_detail(request, pk):
    if request.method == "GET":
        try:
            shoe = Shoe.objects.get(id=pk)
            return JsonResponse(
                shoe, 
                encoder=ShoeDetailEncoder,
                safe=False,
                )
        except Shoe.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid Shoe, verify the information and try again!"},
                status=400,
            )
    elif request.method == "DELETE":
        count, _ = Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"deleted": count > 0})
    else:
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        shoe = Shoe.objects.get(id=pk)
        return JsonResponse(
            shoe,
            encoder=ShoeDetailEncoder,
            safe=False
        )
