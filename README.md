# Wardrobify

Team:

* Henry Tran - Hats
* Tyler Male - Shoes

## Design

Shoes and Hats are microservices which are children of Wardrobe. Shoe and Hats will each have a one-to-many relationship with Wardrobe.

## Shoes microservice

This microservice will have a Shoe model with manufacturer, model_name, color, picture_URL, and the bin in which it exists within the parent, wardrobe.  The data will be coming from the RESTful API which will be wired to the react app.

## Hats microservice

This microservice will have a Hat model with fabric, style_name, color, picture_URL, and the location in which it exists within the parent, wardrobe.  The data will be coming from the RESTful API which will be wired to the react app.
